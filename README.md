# term-tools #

Scripts para automatizar tarefas em sistemas UNIX

## restperm ##

Restaura as permissões padrão em arquivos e diretórios (Ubuntu/Debian). Se nenhum parâmetro for informado, aplica as permissões padrão no diretório atual.
```
Usage: restperm <folder|file>
```

## bkpthis ##

Cria uma cópia da pasta/arquivo com o sufixo '.bkp'. No caso de diretório, este deve ser informado sem a barra (trailing slash).
```
Usage: bkpthis [folder|file]
```

## gitbuild ##

Para diretórios de trabalho com o Git. Cria uma pasta chamada 'builds' (se não existir) no diretório atual e cria um build correspondente à tag informada. Para o segundo parâmetro (opcional), pode ser informado o branch em que o HEAD irá retornar ao final do build. Caso este não seja informado, retorna automaticamente para o branch master.
```
Usage: gitbuild [tag] <branch>
```

## dotsync ##

Sincroniza o diretório atual com o diretório informado no parâmetro (destino) utilizando rsync. Atenção: esse script roda o rsync com a opção '--delete', por isso as exclusões locais são replicadas no destino. Ao incluir um arquivo com o nome '.exclude' na raiz de seu projeto, os arquivos e diretórios listados nele não serão sincronizados.
```
Usage: dotsync [destino]
```